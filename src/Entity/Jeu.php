<?php

namespace App\Entity;

use App\Repository\JeuRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JeuRepository::class)]
class Jeu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $UtilisateurScore = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $Date = null;

    #[ORM\ManyToOne(inversedBy: 'jeux')]
    private ?Utilisateurs $parent = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUtilisateurScore(): ?int
    {
        return $this->UtilisateurScore;
    }

    public function setUtilisateurScore(int $UtilisateurScore): self
    {
        $this->UtilisateurScore = $UtilisateurScore;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getParent(): ?Utilisateurs
    {
        return $this->parent;
    }

    public function setParent(?Utilisateurs $parent): self
    {
        $this->parent = $parent;

        return $this;
    }
}
